![Banner](http://www.madarme.co/portada-web.png)

# Curriculum Vitae

### Biografia
## Indice

1. [Caracteristicas del proyecto](#caracteristicas)
2. [Contenido del proyecto](#contenido)
3. [Tecnologias del proyecto](#caracteristicas)
4. [IDE](#ide)
5. [Instalacion del IDE](#instalacion)
6. [DEMO](#demo)
7. [Autores](#autores-1)
8. [Institucion academica](#institucion-academica)

### Caracteristicas
- Uso del csss Recomendado: [Ver](https://gitlab.com/programacion-web---i-sem-2019/ufps_css)
### Contenido del proyecto
- Index: [ver](https://gitlab.com/santiagoandressp/biography-project/-/blob/master/index.html])
### Tecnologias del proyecto
- Boostrap [ver](https://getbootstrap.com/)
### IDE
- Sublimetext [ver](https://www.sublimetext.com/)
### Instalacion
- Guia de instalacion [ver video](#)
### DEMO

- Aqui puede visualizar un demo del sitio web [ver](https://santiagoandressp.gitlab.io/biography-project)

### Autores
- Santiago Andrés Serrano Pico *codigo: 1151867*
### Institucion academica
- Universidad Francisco de paula Santander [ver sitio web](https://ww2.ufps.edu.co/)

